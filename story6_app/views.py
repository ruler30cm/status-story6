import json
import requests
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponseNotAllowed, HttpResponse
from story6_app.models import Status
from . import forms
from django.contrib.auth import logout
from django.urls import reverse

# Create your views here.
def index(request):
    if request.method == "POST":
        form = forms.FormStatus(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = forms.FormStatus()

    status_list = Status.objects.all().order_by('-datetime')
    return render(request, 'index.html', {'form' : form, 'status' : status_list})

def profile(request):
    return render(request, 'profile.html')

def books(request):
    favorite_count = request.session.get('favorite_count', 0)
    request.session['favorite_count'] = favorite_count
    print(request.session.keys())
    context = {
        'favorite_count' : favorite_count,
    }
    return render(request, 'books.html', context=context)

def book_search(judul):
    judul_baru = ""
    if judul != "-":
        judul_baru = judul
    url = "https://www.googleapis.com/books/v1/volumes?q=" + judul_baru
    request = requests.get(url)
    response = request.json()

    return response

def books_json(request, judul):
    bookjson = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + judul)
    json_data = bookjson.json()
    return JsonResponse(json_data)

def request_logout(request):
    logout(request)
    return HttpResponseRedirect('/books/')

def update_favorite(request):
    if not request.method == "GET":
        return HttpResponseNotAllowed(["GET"])
    
    print(request.session.keys())
    book_id = request.GET.get('bookid')
    clicked = request.GET.get('click')
    print(clicked)
    if clicked == "true":
        request.session['favorite_count'] += 1
    else:
        request.session['favorite_count'] -= 1
    
    return JsonResponse({"fav" : request.session['favorite_count']}, safe=False)

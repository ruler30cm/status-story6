sessionStorage.setItem("ifClicked", {})
var ifClicked = sessionStorage.getItem("ifClicked");

$(function(){ 

    getBooks();

    var condition = true;
    $("#thememode").click(function(){
        $("body").toggleClass("nightmode");
        if (condition) {
            $("h1,h2,p").css("color", "#ffffff");
            $(".ui-widget-content").css("background", "#243447");
            condition = false;
        } else {
            $("h1,h2,p").css("color", "#47525E");
            $(".ui-widget-content").css("background", "#ffffff");
            condition = true;
        }
    });

    $("#accordion").accordion({
        animate: true,
        collapsible: true,
        heightStyle: top,
        active: false
    });

    $('.favorite').hover(function(){
        $(this).attr('src', bintangkejora);
    },function(){
            $(this).attr('src', bintang); 
    });

    $('#search').click(function(){
        getBooks();
    });

    $('#searchbook').keypress(function(e){
        if(e.which == 13){//Enter key pressed
            $('#search').click();//Trigger search button click event
        }
    });

    var lastBookLoad = 0;

    function getBooks() {
        var search_word = $("#searchbook").val();
        console.log(search_word);
        ifClicked = {};

        $("#bookstable").empty();
        favorite = 0;
        $.ajax({
            type: "GET",
            url: "/books/.json/" + search_word,
            data: search_word,
            success: function(data) {
                var tr=[];
                var books = data.items;
                tr.push("<tr><th>Cover</th><th>Judul Buku</th><th>Author(s)</th><th>Publisher</th><th>Tahun</th><th>Kategori</th><th>Add to Favorite</th></tr>");
                for (lastBookLoad; lastBookLoad < books.length; lastBookLoad++) {
                    var favid = "fav" + books[lastBookLoad].id;
                    ifClicked[favid] = false;
                    tr.push("<tr>");
                    if (books[lastBookLoad].volumeInfo.imageLinks != undefined) {
                        tr.push("<td><img src='" + books[lastBookLoad].volumeInfo.imageLinks.smallThumbnail + "'></td>");
                    } else {
                        tr.push("<td>No Cover</td>");
                    }
                    if (books[lastBookLoad].volumeInfo.subtitle != undefined) {
                        tr.push("<td>" + books[lastBookLoad].volumeInfo.title + " - " + books[lastBookLoad].volumeInfo.subtitle + "</td>");
                    } else {
                        tr.push("<td>" + books[lastBookLoad].volumeInfo.title + "</td>");
                    }
                    if (books[lastBookLoad].volumeInfo.authors != undefined) {
                        if (books[lastBookLoad].volumeInfo.authors.length > 1) {
                            tr.push("<td>")
                            for (var i = 0; i < books[lastBookLoad].volumeInfo.authors.length; i++) {
                                tr.push(books[lastBookLoad].volumeInfo.authors[i] + ", ");
                                if (i == books[lastBookLoad].volumeInfo.authors[i]-1) {
                                    tr.push(books[lastBookLoad].volumeInfo.authors[i]);
                                }
                            }
                            tr.push("</td>");
                        } else {
                            tr.push("<td>" + books[lastBookLoad].volumeInfo.authors[0] + "</td>");
                        }
                    } else {
                        tr.push("<td>-</td>");
                    }
                    if (books[lastBookLoad].volumeInfo.publisher != undefined) {
                        tr.push("<td>" + books[lastBookLoad].volumeInfo.publisher + "</td>");
                    } else {
                        tr.push("<td>Unknown</td>");
                    }
                    if (books[lastBookLoad].volumeInfo.publishedDate != undefined) {
                        tr.push("<td>" + books[lastBookLoad].volumeInfo.publishedDate + "</td>");
                    } else {
                        tr.push("<td>-</td>");
                    }
                    if (books[lastBookLoad].volumeInfo.categories != undefined) {
                        if (books[lastBookLoad].volumeInfo.categories.length > 1) {
                            for (var i = 0; i < books[lastBookLoad].volumeInfo.categories.length; i++) {
                                tr.push("<td>" + books[lastBookLoad].volumeInfo.categories[i] + "<br>");
                                if (i == books[lastBookLoad].volumeInfo.categories[i]-1) {
                                    tr.push("<td>" + books[lastBookLoad].volumeInfo.categories[i]);
                                }
                            }
                            tr.push("</td>");
                        } else {
                            tr.push("<td>" + books[lastBookLoad].volumeInfo.categories[0] + "</td>");
                        }
                    } else {
                        tr.push("<td>-</td>");
                    }
                    
                    tr.push("<td><img class='favorite' id='" + favid + "' src='" + bintang + "' onmouseover='hover(this);' onmouseout='unhover(this);' onclick='clicked(this);' ></td>");
                    // add to fav TODO
                }
                lastBookLoad = 0;
                $('#bookstable').append($(tr.join('')));
            }
        })
    }
});

// $(function() {
//     var favorite = 0;

//     $(".favorite").click(function(){
//         if (ifClicked.get($(this).attr('id')) == false) {
//             favorite++;
//             ifClicked.set($(this).attr('id'), true);
//         } else {
//             favorite--;
//         }
//         $(".centered").html(favorite);
//     })
// })

// var favorite = 0;
// function clicked(element) {
//     var counter = document.getElementById("counterfav");
//     var bookid = element.id;
//     if (ifClicked[element.id] == false) {
//         $.ajax({
//             type: "POST",
//             url: "/update-session/",
//             data: {
//                 "bookid" : bookid,
//                 "click" : true,
//             },
//             success: function(data) {
//                 console.log("updated ++");
//             }
//         })
//         element.setAttribute('src', favstar);
//         // favorite++;
//         ifClicked[element.id] = true;
//     } else {
//         $.ajax({
//             type: "POST",
//             url: "/update-session/",
//             data: {
//                 "bookid" : bookid,
//                 "click" : false,
//             },
//             success: function(data) {
//                 console.log("updated --");
//             }
//         })
//         element.setAttribute('src', bintang);
//         // favorite--;
//         ifClicked[element.id] = false;
//     }
//     counter.innerHTML = favorite;
// }

// function hover(element) {
//     if (ifClicked[element.id] == false) {
//         element.setAttribute('src', bintangkejora);
//     }
// }
  
// function unhover(element) {
//     if (ifClicked[element.id] == false) {
//         element.setAttribute('src', bintang);
//     }
    
// }

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 2000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myFooter").style.display = "block";
  document.getElementById("myDiv").style.display = "block";
}
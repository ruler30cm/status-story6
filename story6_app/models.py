from django.db import models
from django.utils.timezone import now

# Create your models here.
class Status(models.Model):
    status = models.CharField(max_length=200)
    datetime = models.DateTimeField(default=now, blank=True)
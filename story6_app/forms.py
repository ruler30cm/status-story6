from django import forms
from .models import Status

class FormStatus(forms.ModelForm):
    status = forms.CharField(max_length=500, widget=forms.Textarea(attrs={'cols': 80, 'rows': 3}))

    class Meta:
        model = Status

        fields = ('status', )

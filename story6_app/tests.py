from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils.timezone import now
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
import json
import requests
from .views import index, profile, books, books_json
from .forms import FormStatus
from .models import Status

# Create your tests here.
class VisitorTest(TestCase):

    def test_if_hello_is_exist(self):
        test = "Hello, apa kabar?"
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_if_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_if_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_if_url_is_not_exist(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code, 404)

    def test_if_post_success_and_render_the_result(self):
        test = 'Lagi makan nih'
        response_post = Client().post('/', {'status' : test, 'datetime' : now})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_post_failed_and_render_the_result(self):
        test = 'Lagi makan nih'
        response_post = Client().post('/', {'status' : '', 'datetime' : ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_form_validation_for_blank_items(self):
            form = FormStatus(data={'status' : ''})
            self.assertFalse(form.is_valid())
            self.assertEqual(
                form.errors['status'],
                ["This field is required."]
            )

    def test_models_if_on_the_database(self):
        new_activity = Status.objects.create(status='lagi ngerjain story ppw nih!', datetime=now())

        counting_all_available_todo = Status.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_if_using_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_if_using_profile_html(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_if_photo_is_exist(self):
        photo = "https://pbs.twimg.com/profile_images/1053659179854884864/M3m-Y6cE_400x400.jpg"
        response= Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(photo, html_response)

    def test_if_name_is_exist(self):
        nama = "Muhammad Rizki Darmawan"
        response= Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertIn(nama, html_response)

    def test_if_using_books_function(self):
        found = resolve('/books/')
        self.assertEqual(found.func, books)

    def test_if_using_books_json_function(self):
        found = resolve('/books/.json/quilting')
        self.assertEqual(found.func, books_json)

    def test_if_table_is_exist(self):
        tags = "</table>"
        response = Client().get('/books/')
        html_response = response.content.decode('utf8')
        self.assertIn(tags, html_response)

    def test_if_url_is_json(self):
        response = requests.get('http://127.0.0.1:8000/books/.json/quilting')
        json_response = response.json()
        self.assertIn("items", json_response)

    def test_if_json_is_same(self):
        response = Client().get('/books/.json/quilting')
        self.assertEqual(response.status_code, 200)
        
    def test_case_connection(self):
        req = requests.get(url='http://127.0.0.1:8000/books/.json/quilting')
        data = json.loads(req.content)
        assert len(data["items"]) == 10



class SeleniumFunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser = webdriver.Chrome("./chromedriver", chrome_options = chrome_options)
        self.browser.implicitly_wait(25) 
        super(SeleniumFunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.implicitly_wait(10)
        self.browser.quit()
        super(SeleniumFunctionalTest,self).tearDown()

    def test_if_status_updater_working(self):
        self.browser.get("http://127.0.0.1:8000/")
        
        form = self.browser.find_element_by_id("id_status")

        submit = self.browser.find_element_by_name("submit")

        text = "Coba Coba"
        form.send_keys(text)

        submit.send_keys(Keys.RETURN)

        status_list = self.browser.find_element_by_tag_name("td").text

        self.assertIn(text, status_list)

    def test_if_change_theme_is_working(self):
        self.browser.get("http://127.0.0.1:8000/profile/")

        button = self.browser.find_element_by_id("thememode")

        button.click()

        background = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")

        self.assertEqual("rgba(20, 29, 38, 1)", background)

    def test_if_update_status_button_in_profile_is_working(self):
        self.browser.get("http://127.0.0.1:8000/profile/")

        button = self.browser.find_element_by_name("update")

        button.click()

        time.sleep(3)

        self.assertEqual("http://127.0.0.1:8000/", self.browser.current_url)

    def test_if_accordion_is_working(self):
        self.browser.get("http://127.0.0.1:8000/profile")

        accordion = self.browser.find_element_by_tag_name("h3")

        time.sleep(5)

        accordion.click()

        self.assertIn(accordion.text, "Aktivitas")

    def test_if_loading_is_working(self):
        self.browser.get("http://127.0.0.1:8000/profile")

        spinner = self.browser.find_element_by_id("loader")

        self.assertIn("spin", spinner.value_of_css_property("animation"))
        self.assertIn("block", spinner.value_of_css_property("display"))

        time.sleep(2)

        page_loaded_footer = self.browser.find_element_by_id("myFooter").text

        page_loaded_body_div = self.browser.find_element_by_id("myDiv").text

        self.assertIn("Muhammad Rizki Darmawan", page_loaded_footer)
        self.assertIn("Muhammad Rizki Darmawan", page_loaded_body_div)

    def test_if_element_h1_exist(self):
        self.browser.get("http://rizdar.herokuapp.com/")

        heading = self.browser.find_element_by_tag_name("h1").text

        self.assertEqual(heading, "Hi, I'm Rizdar")

    def test_if_element_button_exist(self):
        self.browser.get("http://rizdar.herokuapp.com/schedule/add")

        button = self.browser.find_element_by_tag_name("button").text

        self.assertEqual(button, "Save Schedule")

    def test_if_class_desc_exist_and_font_color_is_grey(self):
        self.browser.get("http://rizdar.herokuapp.com/")

        color = self.browser.find_element_by_css_selector("div.desc").value_of_css_property("color")

        self.assertEqual("rgba(71, 82, 94, 1)", color)

    def test_if_class_container_fluid_width_100_percent(self):
        self.browser.get("http://rizdar.herokuapp.com/")

        text_align = self.browser.find_element_by_css_selector("p.text-center").value_of_css_property("text-align")

        self.assertIn("center", text_align)